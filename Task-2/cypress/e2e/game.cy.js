/// <reference types="cypress" />
describe("Приложение пары", () => {
    beforeEach(() => {
        cy.visit("http://localhost:3000");
    });

    it("Нажатие на произвольную карту", () => {
        cy.get("input").type("4");
        cy.contains("Начать игру").click();
        cy.get("li:first-child").click();
        cy.get("li:first-child").should("have.class", "open");
    });

    it("Проверка нахождения пары", () => {
        cy.get("input").type("4");
        cy.contains("Начать игру").click();
        cy.get("li").then((element) => {
            let openCard = [];
            for (const el of element) {
                cy.wait(750);
                if (openCard.length >= 2) {
                    if (openCard[0].textContent === openCard[1].textContent) {
                        cy.get(openCard[0]).should("have.class", "success");
                        cy.get(openCard[1]).should("have.class", "success");
                        return;
                    } else {
                        openCard = [];
                        cy.get(element[0]).click();
                        openCard.push(element[0]);
                        cy.get(el).click();
                        openCard.push(el);
                    }
                } else {
                    cy.get(el).click();
                    openCard.push(el);
                }
            }
        });
    });

    it("Проверка нахождения непарной карты", () => {
        cy.get("input").type("4");
        cy.contains("Начать игру").click();
        cy.get("li").then((element) => {
            let openCard = [];
            for (const el of element) {
                cy.wait(750);
                if (openCard.length >= 2) {
                    if (openCard[0].textContent === openCard[1].textContent) {
                        openCard = [];
                        cy.get(element[0]).click();
                        openCard.push(element[0]);
                        cy.get(el).click();
                        openCard.push(el);
                    } else {
                        cy.get(openCard[0]).should("not.have.class", "open");
                        cy.get(openCard[1]).should("not.have.class", "open");
                        return;
                    }
                } else {
                    cy.get(el).click();
                    openCard.push(el);
                }
            }
        });
    });
});
