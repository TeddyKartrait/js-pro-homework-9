import valid from 'card-validator';
import { el, setChildren } from 'redom';

export function isValidCardNumber(str) {
    return valid.number(str).isValid;
}

export function isValidCVV(str) {
    return valid.cvv(str).isValid;
}

export function isValidDOM(obj) {
    const form = el('form');
    setChildren(form, [obj.cardNumber, obj.date, obj.num, obj.email]);
    return form.outerHTML;
}
