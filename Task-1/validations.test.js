import { isValidCardNumber, isValidCVV, isValidDOM } from './validations.js';
import { createDOMInp } from './src/index.js';

test('Валидация номера карты пропускает корректный номер карты', () => {
    expect(isValidCardNumber('4574487405351567')).toBe(true);
});

test('Валидация номера карты не пропускает произвольную строку, содержащую любые нецифровые символы', () => {
    expect(isValidCardNumber('457448740лмлЛтkn')).toBe(false);
});

test('Валидация номера карты не пропускает строку с недостаточным количеством цифр', () => {
    expect(isValidCardNumber('4574487405')).toBe(false);
});

test('Валидация номера карты не пропускает строку со слишком большим количеством цифр', () => {
    expect(isValidCardNumber('4574487405457448740545744')).toBe(false);
});

test('Валидация CVV/CVC пропускает строку с тремя цифровыми символами', () => {
    expect(isValidCVV('910')).toBe(true);
});

test('Валидация CVV/CVC не пропускает строки с 1-2 цифровыми символами', () => {
    expect(isValidCVV('91')).toBe(false);
});

test('Валидация CVV/CVC не пропускает строки с 4+ цифровыми символами', () => {
    expect(isValidCVV('9111')).toBe(false);
});

test('Валидация CVV/CVC не пропускает строки с тремя нецифровыми символами', () => {
    expect(isValidCVV('kvn')).toBe(false);
});

test('Функция создания DOM-дерева должна вернуть DOM-элемент, в котором содержится строго четыре поля для ввода с плейсхолдерами «Номер карты», «ММ/ГГ», CVV/CVC, Email', () => {
    const expectedText =
        '<form><input placeholder="Номер карты"><input placeholder="ММ/ГГ"><input placeholder="CVV/CVC"><input placeholder="Email"></form>';
    const obj = createDOMInp(),
        el = isValidDOM(obj);
    expect(el).toBe(expectedText);
});
