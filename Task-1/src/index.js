import 'babel-polyfill';
import { el, setChildren } from 'redom';
import valid from 'card-validator';
import './style.css';

export function createDOMInp() {
    const cardNumber = el('input', {
            placeholder: 'Номер карты',
        }),
        date = el('input', {
            placeholder: 'ММ/ГГ',
        }),
        num = el('input', { placeholder: 'CVV/CVC' }),
        email = el('input', {
            placeholder: 'Email',
        });

    return { cardNumber, date, num, email };
}

const form = el('form.form', { action: '#' }),
    btnPay = el('button.btn.disabled', 'Оплатить', { type: 'submit' }),
    wrapper = el('div.wrapper'),
    obj = createDOMInp(),
    errWrapper = el('div.error-wrapper'),
    cardTypeSpan = el('span.card-type-span'),
    errorArray = [];

setChildren(form, [obj.cardNumber, obj.date, obj.num, obj.email, btnPay]);
setChildren(wrapper, cardTypeSpan, errWrapper, form);
setChildren(document.body, wrapper);

obj.cardNumber.addEventListener('input', function (event) {
    errWrapper.innerHTML = '';

    if (valid.number(obj.cardNumber.value).card) {
        checkTypeCard(valid.number(obj.cardNumber.value).card.type);
    } else {
        if (cardTypeSpan.innerHTML !== '') {
            cardTypeSpan.innerHTML = '';
        }
    }

    event.target.value = event.target.value
        .replace(/(\d{4})(?!\s|$)/gm, `$1 `)
        .match(/(?:\d{4} ?){0,3}(?:\d{0,4})?/);

    if (btnPay.classList.contains('disabled')) {
        return;
    } else btnPay.classList.add('disabled');
});

function checkTypeCard(type) {
    if (type === 'visa') {
        setChildren(
            cardTypeSpan,
            el('img.logo', { src: './src/assets/logo-visa.png' })
        );
    }

    if (type === 'mastercard') {
        setChildren(
            cardTypeSpan,
            el('img.logo', { src: './src/assets/logo-mastercard.png' })
        );
    }

    if (type === 'unionpay') {
        setChildren(
            cardTypeSpan,
            el('img.logo', { src: './src/assets/logo-unionpay.png' })
        );
    }
}

obj.cardNumber.addEventListener('blur', () => {
    if (valid.number(obj.cardNumber.value).isValid === false) {
        if (errorArray.includes('cardNumber')) {
            checkError(errorArray);
            return;
        } else errorArray.push('cardNumber');
    } else {
        errorArray.splice(errorArray.indexOf('cardNumber'), 1);
    }

    checkError(errorArray);
});

obj.date.addEventListener('input', function (event) {
    errWrapper.innerHTML = '';

    const inputLength = event.target.value.length;
    if (inputLength === 2) {
        event.target.value += '/';
    }

    if (btnPay.classList.contains('disabled')) {
        return;
    } else btnPay.classList.add('disabled');
});

obj.date.addEventListener('blur', () => {
    const res = obj.date.value.split('/');

    let mounthValidation = valid.expirationMonth(res[0]),
        yearValidation = valid.expirationYear(res[1]);

    if (
        mounthValidation.isValid === false ||
        yearValidation.isValid === false
    ) {
        if (errorArray.includes('date')) {
            checkError(errorArray);
            return;
        } else errorArray.push('date');
    } else {
        errorArray.splice(errorArray.indexOf('date'), 1);
    }

    checkError(errorArray);
});

obj.num.addEventListener('input', function (event) {
    event.target.value = event.target.value.replace(/[^\d]/, '');

    if (btnPay.classList.contains('disabled')) {
        return;
    } else btnPay.classList.add('disabled');
});

obj.num.addEventListener('blur', () => {
    if (valid.cvv(obj.num.value).isValid === false) {
        if (errorArray.includes('num')) {
            checkError(errorArray);
            return;
        } else {
            errorArray.push('num');
        }
    } else {
        errorArray.splice(errorArray.indexOf('num'), 1);
    }

    checkError(errorArray);
});

obj.email.addEventListener('input', () => {
    if (btnPay.classList.contains('disabled')) {
        return;
    } else btnPay.classList.add('disabled');
});

obj.email.addEventListener('blur', () => {
    if (obj.email.value.length < 2 || obj.email.value.includes('@') === false) {
        if (errorArray.includes('email')) {
            checkError(errorArray);
            return;
        } else {
            errorArray.push('email');
        }
    } else {
        errorArray.splice(errorArray.indexOf('email'), 1);
    }

    checkError(errorArray);
});

function checkError(arr) {
    for (const element of arr) {
        if (element === 'cardNumber') {
            setChildren(
                errWrapper,
                el('span.error', 'Карты с таким номером не существует')
            );
            return;
        }

        if (element === 'date') {
            setChildren(
                errWrapper,
                el('span.error', 'Дата введена неккоректно')
            );
            return;
        }

        if (element === 'num') {
            setChildren(
                errWrapper,
                el('span.error', 'Номер должен содержать три цифры')
            );
            return;
        }

        if (element === 'email') {
            setChildren(
                errWrapper,
                el(
                    'span.error',
                    'Email должен содерать не менее 2-х симолов и иметь символ "@"'
                )
            );
            return;
        }
    }

    if (
        obj.cardNumber.value !== '' &&
        obj.num.value !== '' &&
        obj.email.value !== '' &&
        obj.date.value !== ''
    )
        btnPay.classList.remove('disabled');
}
